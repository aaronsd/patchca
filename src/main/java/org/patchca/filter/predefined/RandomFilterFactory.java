package org.patchca.filter.predefined;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.patchca.filter.FilterFactory;

public class RandomFilterFactory implements FilterFactory {
	private final List<FilterFactory>	filterFactories	= new ArrayList<FilterFactory>();
	private final Random				random			= new Random();

	public void addFilterFactory(FilterFactory filterFactory) {
		this.filterFactories.add(filterFactory);
	}

	@Override
	public BufferedImage applyFilters(BufferedImage source) {
		FilterFactory filterFactory = this.filterFactories.get(this.random.nextInt(this.filterFactories.size()));
		return filterFactory.applyFilters(source);
	}
}
