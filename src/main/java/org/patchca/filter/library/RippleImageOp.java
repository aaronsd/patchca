/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.filter.library;

public class RippleImageOp extends AbstractTransformImageOp {

	protected double	xWavelength;
	protected double	yWavelength;
	protected double	xAmplitude;
	protected double	yAmplitude;
	protected double	xRandom;
	protected double	yRandom;

	public RippleImageOp() {
		this.xWavelength = 20;
		this.yWavelength = 10;
		this.xAmplitude = 5;
		this.yAmplitude = 5;
		this.xRandom = 5 * Math.random();
		this.yRandom = 5 * Math.random();
	}

	public double getxWavelength() {
		return this.xWavelength;
	}

	public void setxWavelength(double xWavelength) {
		this.xWavelength = xWavelength;
	}

	public double getyWavelength() {
		return this.yWavelength;
	}

	public void setyWavelength(double yWavelength) {
		this.yWavelength = yWavelength;
	}

	public double getxAmplitude() {
		return this.xAmplitude;
	}

	public void setxAmplitude(double xAmplitude) {
		this.xAmplitude = xAmplitude;
	}

	public double getyAmplitude() {
		return this.yAmplitude;
	}

	public void setyAmplitude(double yAmplitude) {
		this.yAmplitude = yAmplitude;
	}

	@Override
	protected void transform(int x, int y, double[] t) {
		double tx = Math.sin(y / this.yWavelength + this.yRandom);
		double ty = Math.cos(x / this.xWavelength + this.xRandom);
		t[0] = x + this.xAmplitude * tx;
		t[1] = y + this.yAmplitude * ty;
	}

}
