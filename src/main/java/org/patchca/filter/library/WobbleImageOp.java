/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.filter.library;

public class WobbleImageOp extends AbstractTransformImageOp {

	private double			xWavelength;
	private double			yWavelength;
	private double			xAmplitude;
	private double			yAmplitude;
	private final double	xRandom;
	private final double	yRandom;
	private double			xScale;
	private double			yScale;

	public WobbleImageOp() {
		this.xWavelength = 15;
		this.yWavelength = 15;
		this.xAmplitude = 4.0;
		this.yAmplitude = 3.0;
		this.xScale = 1.0;
		this.yScale = 1.0;
		this.xRandom = 3 * Math.random();
		this.yRandom = 10 * Math.random();
	}

	public double getxWavelength() {
		return this.xWavelength;
	}

	public void setxWavelength(double xWavelength) {
		this.xWavelength = xWavelength;
	}

	public double getyWavelength() {
		return this.yWavelength;
	}

	public void setyWavelength(double yWavelength) {
		this.yWavelength = yWavelength;
	}

	public double getxAmplitude() {
		return this.xAmplitude;
	}

	public void setxAmplitude(double xAmplitude) {
		this.xAmplitude = xAmplitude;
	}

	public double getyAmplitude() {
		return this.yAmplitude;
	}

	public void setyAmplitude(double yAmplitude) {
		this.yAmplitude = yAmplitude;
	}

	public double getxScale() {
		return this.xScale;
	}

	public void setxScale(double xScale) {
		this.xScale = xScale;
	}

	public double getyScale() {
		return this.yScale;
	}

	public void setyScale(double yScale) {
		this.yScale = yScale;
	}

	@Override
	protected void transform(int x, int y, double[] t) {
		double tx = Math.cos((this.xScale * x + y) / this.xWavelength + this.xRandom);
		double ty = Math.sin((this.yScale * y + x) / this.yWavelength + this.yRandom);
		t[0] = x + this.xAmplitude * tx;
		t[1] = y + this.yAmplitude * ty;

	}

}
