/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.filter.library;

import java.util.Random;

public class DiffuseImageOp extends AbstractTransformImageOp {

	double[]	tx;
	double[]	ty;
	double		amount;

	public DiffuseImageOp() {
		this.amount = 1.6;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	protected synchronized void init() {
		this.tx = new double[256];
		this.ty = new double[256];
		for (int i = 0; i < 256; i++) {
			double angle = 2 * Math.PI * i / 256;
			this.tx[i] = this.amount * Math.sin(angle);
			this.ty[i] = this.amount * Math.cos(angle);
		}
	}

	@Override
	protected void transform(int x, int y, double[] t) {
		Random r = new Random();
		int angle = (int) (r.nextFloat() * 255);
		t[0] = x + this.tx[angle];
		t[1] = y + this.ty[angle];
	}

}
