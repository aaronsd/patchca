/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.filter.library;

public class DoubleRippleImageOp extends RippleImageOp {

	@Override
	protected void transform(int x, int y, double[] t) {
		double tx = Math.sin(y / this.yWavelength + this.yRandom) + 1.3 * Math.sin(0.6 * y / this.yWavelength + this.yRandom);
		double ty = Math.cos(x / this.xWavelength + this.xRandom) + 1.3 * Math.cos(0.6 * x / this.xWavelength + this.xRandom);
		t[0] = x + this.xAmplitude * tx;
		t[1] = y + this.yAmplitude * ty;
	}

}
