package org.patchca.custom;

import java.util.ArrayList;

import org.patchca.random.RandUtils;
import org.patchca.random.StrUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

public class TranslateEnglishWordFactory implements WordFactory {
	private static ArrayList<String>	chinese;
	private static ArrayList<String>	english;

	static {
		String str = StrUtils.loadClasspathResourceToString("/org/patchca/custom/englishwords.txt");
		String[] kvs = str.split("\\r\\n");
		chinese = new ArrayList<String>(kvs.length);
		english = new ArrayList<String>(kvs.length);
		for (String kv : kvs) {
			String[] pair = kv.split(" ");
			if (pair.length == 2) {
				chinese.add(pair[0]);
				english.add(pair[1]);
			}
		}
	}

	@Override
	public WordBean getNextWord() {
		int nextInt = RandUtils.randInt(chinese.size());
		String word = chinese.get(nextInt);
		String answer = english.get(nextInt);
		return new WordBean(word, answer, "请翻译中文为英文单词");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return null;
	}

}
