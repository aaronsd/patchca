/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.custom;

import org.patchca.background.SingleColorBackgroundFactory;
import org.patchca.color.SingleColorFactory;
import org.patchca.filter.predefined.CurvesRippleFilterFactory;
import org.patchca.font.RandomFontFactory;
import org.patchca.service.AbstractCaptchaService;
import org.patchca.text.renderer.BestFitTextRenderer;
import org.patchca.word.AdaptiveRandomWordFactory;

/**
 * 配合工厂
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class ConfigurableCaptchaService extends AbstractCaptchaService {

	public ConfigurableCaptchaService() {
		this.backgroundFactory = new SingleColorBackgroundFactory();
		this.wordFactory = new AdaptiveRandomWordFactory();
		this.fontFactory = new RandomFontFactory();
		this.textRenderer = new BestFitTextRenderer();
		this.colorFactory = new SingleColorFactory();
		this.filterFactory = new CurvesRippleFilterFactory(this.colorFactory);
		this.textRenderer.setLeftMargin(10);
		this.textRenderer.setRightMargin(10);
		this.width = 160;
		this.height = 70;
	}

	// public ConfigurableCaptchaService() {
	// backgroundFactory = new SingleColorBackgroundFactory();
	// wordFactory = new RandomFactoryWordFactory(wordFactories);
	// fontFactory = new RandomFontFactory();
	// fontFactory.setWordFactory(wordFactory);
	// textRenderer = new BestFitTextRenderer();
	// colorFactory = new SingleColorFactory();
	//
	// switch (RandUtils.randInt(5)) {
	// case 0:
	// filterFactory = new CurvesRippleFilterFactory(colorFactory);
	// break;
	// case 1:
	// filterFactory = new MarbleRippleFilterFactory(); // 不清楚
	// break;
	// case 2:
	// filterFactory = new DoubleRippleFilterFactory(); // 很清楚
	// break;
	// case 3:
	// filterFactory = new WobbleRippleFilterFactory();
	// break;
	// case 4:
	// filterFactory = new DiffuseRippleFilterFactory();
	// break;
	// }
	//
	// //filterFactory = new CurvesRippleFilterFactory(colorFactory);
	// textRenderer.setLeftMargin(10);
	// textRenderer.setRightMargin(10);
	// width = 400;
	// height = 100;
	// }

	// private static ArrayList<WordFactory> wordFactories = new
	// ArrayList<WordFactory>();
	//
	// static {
	// wordFactories.add(new MathExprFactory()); // 三个单数运算
	// wordFactories.add(new MathArithmeticFactory()); // 四则运算
	// wordFactories.add(new ChineseIdiomFactory()); // 成语
	// wordFactories.add(new ChineseIdiomGuessFactory()); // 成语猜字
	// wordFactories.add(new EnglishWordFactory()); // 常见2000英语单词
	// wordFactories.add(new AdaptiveRandomWordFactory()); // 宽字符只会有一个的随机
	// wordFactories.add(new RandomWordFactory()); // 随机
	// wordFactories.add(new SymbolDiffFactory()); // 符号找不同
	// wordFactories.add(new KnowledgeWordFactory()); // 地理知识
	// wordFactories.add(new AdaptiveRandomWordFactory());
	// wordFactories.add(new RandomChineseFactory()); // 随机汉字
	// wordFactories.add(new RandomChineseJianpinFactory()); // 随机汉字简拼
	// wordFactories.add(new ChineseIdiomJianpingFactory()); // 随机成语简拼
	// wordFactories.add(new RandomChineseQuanpinFactory()); // 随机汉字全拼
	// }
	//
	// public static ArrayList<WordFactory> getWordFactories() {
	// return wordFactories;
	// }
}
