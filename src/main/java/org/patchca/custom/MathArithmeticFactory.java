package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

/**
 * 数学运算WordFactory
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class MathArithmeticFactory implements WordFactory {
	private static String[]	chineseNumber	= new String[] { "零", "1壹①", "2二贰②", "3三叁③", "4四肆④", "5五伍⑤", "6六陆⑥", "7七柒⑦", "8八捌⑧", "9九玖⑨", "十拾⑩", "百佰",
			"千仟", "万" };

	private static String[]	operations		= new String[] { "加+", "减-", "乘×", "除÷" };

	public static String rand(int n) {
		if (n > 9) {
			return "" + n;
		}

		String s1 = chineseNumber[n];
		char c1 = s1.charAt(RandUtils.randInt(s1.length()));
		return "" + c1;
	}

	@Override
	public WordBean getNextWord() {
		int n1 = RandUtils.randInt(9) + 1;
		int n2 = RandUtils.randInt(9) + 1;
		String rand1 = rand(n1);
		String rand2 = rand(n2);
		String randMin = rand(Math.min(n1, n2));
		String randMax = rand(Math.max(n1, n2));
		String rand12 = rand(n1 * n2);

		int op = RandUtils.randInt(4);
		String opStr = operations[op];
		char opCh = opStr.charAt(RandUtils.randInt(opStr.length()));

		int expected = 0;
		String result = "";
		// 未知数的未知
		int askPos = RandUtils.randInt(3);
		if (op == 0) { // +
			if (askPos == 0) {
				expected = Math.abs(n1 - n2);
				result = "?" + opCh + randMin + '=' + randMax;
			} else if (askPos == 1) {
				expected = Math.abs(n1 - n2);
				result = randMin + opCh + "?=" + randMax;
			} else if (askPos == 2) {
				expected = n1 + n2;
				result = rand1 + opCh + rand2 + "=?";
			}
		} else if (op == 1) { // -
			if (askPos == 0) {
				expected = n1 + n2;
				result = "?" + opCh + rand1 + '=' + rand2;
			} else if (askPos == 1) {
				expected = Math.abs(n1 - n2);
				result = randMax + opCh + "?=" + randMin;
			} else if (askPos == 2) {
				expected = Math.abs(n1 - n2);
				result = randMax + opCh + randMin + "=?";
			}
		} else if (op == 2) { // *
			if (askPos == 0) {
				expected = n2;
				result = "?" + opCh + rand1 + '=' + rand12;
			} else if (askPos == 1) {
				expected = n2;
				result = rand1 + opCh + "?=" + rand12;
			} else if (askPos == 2) {
				expected = n2 * n1;
				result = rand1 + opCh + rand2 + "=?";
			}
		} else if (op == 3) { // /
			if (askPos == 0) {
				expected = n2 * n1;
				result = "?" + opCh + rand1 + '=' + rand2;
			} else if (askPos == 1) {
				expected = n2;
				result = rand12 + opCh + "?=" + rand1;
			} else if (askPos == 2) {
				expected = n2;
				result = rand12 + opCh + rand1 + "=?";
			}
		}

		return new WordBean(result, "" + expected, "请输入图片中?代表的数字");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}
}
