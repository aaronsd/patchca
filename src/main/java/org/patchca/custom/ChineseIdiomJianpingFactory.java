package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.random.SpellUtils;
import org.patchca.random.StrUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

/**
 * 输入简拼
 *
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseIdiomJianpingFactory implements WordFactory {
	protected static String[] idioms;

	static {
		String str = StrUtils.loadClasspathResourceToString("/org/patchca/custom/chineseidioms.txt");
		Iterable<String> split = Splitter.onPattern("\\s+").omitEmptyStrings().split(str);
		idioms = Iterables.toArray(split, String.class);

	}

	@Override
	public WordBean getNextWord() {
		int nextInt = RandUtils.randInt(idioms.length);
		String answer = idioms[nextInt];

		return new WordBean(answer, SpellUtils.getFirst(answer), "请输入简拼");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}

}
