package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.word.WordBean;

/**
 * 猜成语 ChineseIdiomFactory
 *
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseIdiomGuessFactory extends ChineseIdiomFactory {

	@Override
	public WordBean getNextWord() {
		WordBean wordBean = super.getNextWord();
		String word = wordBean.getWord();
		int pos = RandUtils.randInt(word.length());

		return new WordBean(word.substring(0, pos) + "?" + word.substring(pos + 1), word.substring(pos, pos + 1), "请输入图片中?的代表的汉字");

	}

}
