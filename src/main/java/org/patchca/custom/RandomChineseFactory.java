package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

/**
 * 随机的3-4个汉字
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class RandomChineseFactory implements WordFactory {
	@Override
	public WordBean getNextWord() {
		String randChinese = RandUtils.randChinese((3 + RandUtils.randInt(2)));

		return new WordBean(randChinese, randChinese, "请输入图片中的文字");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}
}
