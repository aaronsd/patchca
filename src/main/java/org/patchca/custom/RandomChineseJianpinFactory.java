package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.random.SpellUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

public class RandomChineseJianpinFactory implements WordFactory {
	@Override
	public WordBean getNextWord() {
		String randChinese = RandUtils.randChinese(4);

		return new WordBean("请输入“" + randChinese + "”的简拼", SpellUtils.getFirst(randChinese), "请输入简拼");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}
}
