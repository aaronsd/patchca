package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.random.StrUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

/**
 * 中文成语 WordFactory
 *
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseIdiomFactory implements WordFactory {

	protected static String[] idioms;

	static {
		String str = StrUtils.loadClasspathResourceToString("/org/patchca/custom/idoms.txt");
		Iterable<String> split = Splitter.onPattern("\\s+").omitEmptyStrings().split(str);
		idioms = Iterables.toArray(split, String.class);
	}

	@Override
	public WordBean getNextWord() {
		int nextInt = RandUtils.randInt(idioms.length);
		String answer = idioms[nextInt];

		return new WordBean(answer, answer, "请输入图片中的文字");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}

}
