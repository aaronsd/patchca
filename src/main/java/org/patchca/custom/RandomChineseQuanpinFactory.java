package org.patchca.custom;

import org.patchca.random.RandUtils;
import org.patchca.random.SpellUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

public class RandomChineseQuanpinFactory implements WordFactory {
	@Override
	public WordBean getNextWord() {
		String randChinese = RandUtils.randChinese(2);

		return new WordBean("请输入“" + randChinese + "”的全拼", SpellUtils.getFull(randChinese), "请输入全拼");
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return new String[] { "宋体" };
	}
}
