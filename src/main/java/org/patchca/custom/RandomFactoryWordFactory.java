package org.patchca.custom;

import java.util.List;

import org.patchca.random.RandUtils;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

public class RandomFactoryWordFactory implements WordFactory {
	private final List<WordFactory>			factories;
	private static ThreadLocal<WordFactory>	wordFactory	= new ThreadLocal<WordFactory>();

	public RandomFactoryWordFactory(List<WordFactory> factories) {
		this.factories = factories;
	}

	@Override
	public WordBean getNextWord() {
		WordFactory value = this.factories.get(RandUtils.randInt(this.factories.size()));
		wordFactory.set(value);
		return value.getNextWord();
	}

	@Override
	public String[] getSupportedFontFamilies() {
		WordFactory wf = wordFactory.get();
		return wf.getSupportedFontFamilies();
	}

}
