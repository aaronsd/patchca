/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.service;

import java.awt.image.BufferedImage;

public class Captcha {
	public static final String	SESSION_KEY	= "org.patchca.service.Captcha.challenge";
	private String				word;
	private final String		challenge;
	private BufferedImage		image;
	private final String		tips;

	public Captcha(String word, String challenge, BufferedImage image, String tips) {
		this.word = word;
		this.challenge = challenge;
		this.image = image;
		this.tips = tips;
	}

	public String getChallenge() {
		return this.challenge;
	}

	public BufferedImage getImage() {
		return this.image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getTips() {
		return this.tips;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Captcha [word=" + this.word + ", challenge=" + this.challenge + ", tips=" + this.tips + "]";
	}
}
