package org.patchca.word;

public class WordBean {

	private final String	word;
	private final String	answer;

	private final String	tips;

	public WordBean(String word, String answer, String tips) {
		this.word = word;
		this.answer = answer;
		this.tips = tips;
	}

	public String getWord() {
		return this.word;
	}

	public String getAnswer() {
		return this.answer;
	}

	public String getTips() {
		return this.tips;
	}

	@Override
	public String toString() {
		return "WordBean [word=" + this.word + ", answer=" + this.answer + ", tips=" + this.tips + "]";
	}

}
