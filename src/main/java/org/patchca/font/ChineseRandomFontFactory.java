/*
 * Copyright (c) 2009 Piotr Piastucki
 *
 * This file is part of Patchca CAPTCHA library.
 *
 * Patchca is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Patchca is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Patchca. If not, see <http://www.gnu.org/licenses/>.
 */
package org.patchca.font;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.patchca.word.WordFactory;

public class ChineseRandomFontFactory implements FontFactory {
	protected Random		r	= new Random();

	protected List<String>	families;
	protected int			minSize;
	protected int			maxSize;
	private String			word;
	private WordFactory		wordFactory;

	public ChineseRandomFontFactory() {
		this.families = new ArrayList<String>();
		String[] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for (String fontName : fontNames) {
			boolean canDisplay = true;
			try {
				if (fontName.length() == fontName.getBytes("UTF-8").length) {
					// 丢弃纯英文字体名称的字体
					canDisplay = false;
				}
				canDisplay &= canDisplay ? this.canDisplay(fontName, '壹') : false;
				canDisplay &= canDisplay ? this.canDisplay(fontName, '九') : false;
				canDisplay &= canDisplay ? this.canDisplay(fontName, '①') : false;
				canDisplay &= canDisplay ? this.canDisplay(fontName, '+') : false;
				canDisplay &= canDisplay ? this.canDisplay(fontName, '1') : false;
				canDisplay &= canDisplay ? this.canDisplay(fontName, '×') : false;
				if (canDisplay) {
					this.families.add(fontName);
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		}

		this.minSize = 30;
		this.maxSize = 45;
	}

	public boolean canDisplay(String fontName, char ch) {
		Font f = new Font(fontName, Font.PLAIN, 12);
		return f.canDisplay(ch);
	}

	public void setMinSize(int minSize) {
		this.minSize = minSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	@Override
	public Font getFont(int index) {
		Font randFont = null;
		boolean bold = this.r.nextBoolean();

		int size = this.minSize;
		if (this.maxSize - this.minSize > 0) {
			size += this.r.nextInt(this.maxSize - this.minSize);
		}

		if (this.wordFactory != null) {
			String[] supportedFamilies = this.wordFactory.getSupportedFontFamilies();
			if (supportedFamilies != null && supportedFamilies.length > 0) {
				String family = supportedFamilies[this.r.nextInt(supportedFamilies.length)];
				if (this.families.contains(family)) {
					randFont = new Font(family, bold ? Font.BOLD : Font.PLAIN, size);
				}
			}
		}
		// 测试wordFactory的字体是否可用
		boolean canDisplay = false;
		if (randFont != null && this.word != null && this.word.length() > 0) {
			canDisplay = true;
			for (char c : this.word.toCharArray()) {
				canDisplay &= randFont.canDisplay(c);
			}
		}

		// 如果默认支持字体不可用，采用一个随机可用的系统字体(可用系统字体已经采样检测)
		if (randFont == null || !canDisplay) {
			String family = this.families.get(this.r.nextInt(this.families.size()));
			randFont = new Font(family, bold ? Font.BOLD : Font.PLAIN, size);
		}
		return randFont;
	}

	@Override
	public void setWord(String word) {
		this.word = word;
	}

	@Override
	public void setWordFactory(WordFactory wordFactory) {
		this.wordFactory = wordFactory;
	}

}
