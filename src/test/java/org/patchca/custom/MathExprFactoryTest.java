package org.patchca.custom;

import org.junit.Test;
import org.patchca.word.WordBean;

/**
 * 多项式运算
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class MathExprFactoryTest {

	@Test
	public void test() {
		MathExprFactory factory = new MathExprFactory();
		int k = 100;
		while (--k > 0) {
			WordBean wb = factory.getNextWord();
			System.out.println(wb.toString());
		}
	}

}
