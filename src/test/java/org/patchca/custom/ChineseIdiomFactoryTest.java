package org.patchca.custom;

import org.junit.Test;
import org.patchca.word.WordBean;

/**
 * 成语
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseIdiomFactoryTest {

	@Test
	public void test() {
		ChineseIdiomFactory factory = new ChineseIdiomFactory();
		int k = 100;
		while (--k > 0) {
			WordBean wb = factory.getNextWord();
			System.out.println(wb.toString());
		}
	}

}
