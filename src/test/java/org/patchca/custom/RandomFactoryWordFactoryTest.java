package org.patchca.custom;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

/**
 * 随机文字工厂
 *
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class RandomFactoryWordFactoryTest {

	@Test
	public void test() {
		List<WordFactory> factories = new ArrayList<WordFactory>();
		factories.add(new MathArithmeticFactory()); // 数学运算
		factories.add(new ChineseIdiomGuessFactory()); // 猜成语
		factories.add(new ChineseIdiomFactory()); // 成语

		RandomFactoryWordFactory factory = new RandomFactoryWordFactory(factories);
		int k = 100;
		while (--k > 0) {
			WordBean wb = factory.getNextWord();
			System.out.println(wb.toString());
		}
	}

}
