package org.patchca.word;

import java.util.ArrayList;
import java.util.List;

import org.patchca.custom.ChineseIdiomFactory;
import org.patchca.custom.ChineseIdiomGuessFactory;
import org.patchca.custom.MathArithmeticFactory;
import org.patchca.custom.RandomFactoryWordFactory;
import org.patchca.word.WordBean;
import org.patchca.word.WordFactory;

/**
 * 随机的含中文字的文字工厂
 * 
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseRandomFactory implements WordFactory {
	RandomFactoryWordFactory factory;

	public ChineseRandomFactory() {
		List<WordFactory> factories = new ArrayList<WordFactory>();
		factories.add(new MathArithmeticFactory()); // 数学运算结果
		factories.add(new ChineseIdiomGuessFactory()); // 猜成语缺失字
		factories.add(new ChineseIdiomFactory()); // 输入成语

		factory = new RandomFactoryWordFactory(factories);
	}

	@Override
	public WordBean getNextWord() {
		return this.factory.getNextWord();
	}

	@Override
	public String[] getSupportedFontFamilies() {
		return this.factory.getSupportedFontFamilies();
	}

}
