package org.patchca;

import java.awt.GraphicsEnvironment;

import org.junit.Test;

public class ListJavaFonts {

	@Test
	public void listJavaFonts() {
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String fonts[] = env.getAvailableFontFamilyNames();

		for (String font : fonts) {
			System.out.println("<" + font + ">");
		}
	}

}
