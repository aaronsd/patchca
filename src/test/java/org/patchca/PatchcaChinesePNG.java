package org.patchca;

import java.io.FileOutputStream;

import org.junit.Test;
import org.patchca.service.Captcha;
import org.patchca.service.ChineseRandomCaptchaService;
import org.patchca.utils.encoder.EncoderHelper;

public class PatchcaChinesePNG {
	ChineseRandomCaptchaService crcs = new ChineseRandomCaptchaService();

	@Test
	public void png() throws Exception {
		long start = System.currentTimeMillis();
		for (int counter = 0; counter < 100; counter++) {
			FileOutputStream fos = new FileOutputStream("D:\\img\\patcha_demo" + counter + ".png");
			Captcha captcha = EncoderHelper.getCaptchaAndWriteImage(crcs, "png", fos);
			System.out.println(captcha);
			fos.close();
		}
		System.out.println("平均生成一个图片需要: " + ((System.currentTimeMillis() - start) / 100) + "ms");
	}
}
