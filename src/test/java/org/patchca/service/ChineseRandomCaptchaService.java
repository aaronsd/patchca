package org.patchca.service;

import org.patchca.background.NoiseBackgroundFactory;
import org.patchca.color.RandomColorFactory;
import org.patchca.filter.ConfigurableFilterFactory;
import org.patchca.font.ChineseRandomFontFactory;
import org.patchca.text.renderer.BestFitTextRenderer;
import org.patchca.word.ChineseRandomFactory;



/**
 * 含中文图形验证码生成服务
 * 
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class ChineseRandomCaptchaService extends AbstractCaptchaService {

	public ChineseRandomCaptchaService() {
		this.width = 200;
		this.height = 50;

		this.wordFactory = new ChineseRandomFactory();
		this.fontFactory = new ChineseRandomFontFactory();
		this.fontFactory.setWordFactory(this.wordFactory);
		this.colorFactory = new RandomColorFactory();

		this.textRenderer = new BestFitTextRenderer();
		this.textRenderer.setLeftMargin(10);
		this.textRenderer.setRightMargin(10);

		this.backgroundFactory = new NoiseBackgroundFactory();
		((NoiseBackgroundFactory) this.backgroundFactory).setLineCount(3);
		((NoiseBackgroundFactory) this.backgroundFactory).setNoiseCount(100);

		this.filterFactory = new ConfigurableFilterFactory();// filters 为空
	}
}
