package org.patchca.service;

import org.patchca.background.NoiseBackgroundFactory;
import org.patchca.color.RandomColorFactory;
import org.patchca.filter.predefined.CurvesRippleFilterFactory;
import org.patchca.filter.predefined.DiffuseRippleFilterFactory;
import org.patchca.filter.predefined.DoubleRippleFilterFactory;
import org.patchca.filter.predefined.MarbleRippleFilterFactory;
import org.patchca.filter.predefined.RandomFilterFactory;
import org.patchca.filter.predefined.WobbleRippleFilterFactory;
import org.patchca.font.RandomFontFactory;
import org.patchca.text.renderer.BestFitTextRenderer;
import org.patchca.word.AdaptiveRandomWordFactory;
import org.patchca.word.RandomWordFactory;




/**
 * 含中文图形验证码生成服务
 * 
 * @author aaron
 * @varsion 0.1.0 2017年11月12日
 */
public class LettersAndNumCaptchaService extends AbstractCaptchaService {

	public LettersAndNumCaptchaService() {
		this.width = 150;
		this.height = 50;

		this.backgroundFactory = new NoiseBackgroundFactory();
		((NoiseBackgroundFactory) this.backgroundFactory).setLineCount(3);
		((NoiseBackgroundFactory) this.backgroundFactory).setNoiseCount(100);

		this.fontFactory = new RandomFontFactory();
		((RandomFontFactory) this.fontFactory).setMinSize(40);
		((RandomFontFactory) this.fontFactory).setMaxSize(45);
		((RandomFontFactory) this.fontFactory).setRandomStyle(true);// 支持粗体
		
		this.colorFactory = new RandomColorFactory();

		this.textRenderer = new BestFitTextRenderer();
		this.textRenderer.setLeftMargin(10);
		this.textRenderer.setRightMargin(10);

		this.wordFactory = new AdaptiveRandomWordFactory();
		((RandomWordFactory) this.wordFactory).setCharacters("absdegkmnpwx23456789");
		((RandomWordFactory) this.wordFactory).setMinLength(4);
		((RandomWordFactory) this.wordFactory).setMaxLength(4);

		this.filterFactory = new RandomFilterFactory();
		((RandomFilterFactory) filterFactory).addFilterFactory(new CurvesRippleFilterFactory(this.colorFactory));
		((RandomFilterFactory) filterFactory).addFilterFactory(new MarbleRippleFilterFactory());
		((RandomFilterFactory) filterFactory).addFilterFactory(new DoubleRippleFilterFactory());
		((RandomFilterFactory) filterFactory).addFilterFactory(new WobbleRippleFilterFactory());
		((RandomFilterFactory) filterFactory).addFilterFactory(new DiffuseRippleFilterFactory());

	}
}
